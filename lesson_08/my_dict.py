# my_dict.py
the_goat = {
  "welcome": "\nWelcome to The Goat!\n\nYou are a goat trying to save your family from the butcher.\n\n"\
             "If you get stuck, try typing help\n",

  "help": """\
  You can go places by typing: go north, or go cave
  Try examining things by typing: look, look around, look bird, or look tree
  Get or stop carrying objects by typing: get box, or drop stick
  To see what you are carrying type: inventory
  To quit, type: quit

  Now go explore! Have fun!
  """,

  "inventory": { "bell": "the bell makes a nice little sound" },

  "cur_place": "valley",

  "actions": verbs,

  "places": {

    # adventure starts in a valley
    "valley": { 
      "description": "You are in a valley with tall grass and flowers. Two goats are grazing with you.",

      "exits": { "east": "bridge", "west": "blue door" },

      "objects": { "flowers": "it looks like wild rose", 
        "flower crown": "the crown is made of wild roses", "small rock": "it's a rock",
        "big rock": "it's a bid rock" },

      "actions": { "look": meadow_look, "catch": meadow_catch, "get": meadow_get,
                    "throw": meadow_throw },

      "tags": { }
    }
  }
}