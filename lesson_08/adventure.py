"""
  adventure.py

  This is a general purpose adventure engine for making fun text adventures
"""
import string

# parse object in string
def parse_object(s):
  """ parse object in string """
  i = string.find(s, ' ')
  if i < 0:
    return None
  else:
    return s[i+1:]

# parse verb in string
def parse_verb(s):
  i = string.find(s, ' ')
  if i < 0:
    return s
  else:
    return s[0:i]

# find word prefix (a vs an)
def word_prefix(s):
  if s[0] in "aeiou":
    return "an "
  else:
    return "a "

def print_idontsee(obj):
  print "I don't see " + word_prefix(obj) + "'" + obj + "'"

# show help
def show_help(game, place, obj):
  print game["help"]
  return True

# go to a place
def go_place(game, place, obj):
  if obj == None:
    print "go where?"
  else:
    exits = place["exits"]
    if obj in exits:
      place_name = exits[obj]
      game["cur_place"] = place_name
      print game["places"][place_name]["description"]
    else:
      print "I can't go " + str(obj)
  return True

# quit game
def do_quit(game, place, obj):
  s = raw_input("Are you sure you want to leave this wonderful adventure? ")
  if string.lower(s[0]) == 'y':
    exit()
  return True

# look around
def look(game, place, obj):
  objs = place["objects"]

  # look by itself describes the place again
  if obj == None:
    print place["description"]

  # look around lists objects in the place
  elif obj == "around":
    num_items = len(objs)
    print "You see",
    if num_items == 0:
      print "nothing of importance"
    else:
      for i, key in enumerate(objs.keys()):
        s = ""
        if i+1 < num_items:
          s = ","
        elif i:
          print "and",
        print word_prefix(key) + key + s,

  # look at an object in the place gives it's description
  elif obj in objs:
    print objs[obj]

  # look at an object in the inventory gives it's description
  elif obj in game["inventory"]:
    print game["inventory"][obj]

  else:
    print_idontsee(obj)

  # look is always handled
  return True

# take inventory
def take_inventory(game, place, obj):
  inventory = game["inventory"]
  num_items = len(inventory)
  if num_items == 0:
    print "You are carrying nothing"
  else:
    print "You have",
    for i, key in enumerate(inventory.keys()):
      s = ""
      if i+1 < num_items:
        s = ","
      elif i:
        print "and",
      print word_prefix(key) + key + s,
  return True

# get something
def do_get(game, place, obj):
  if obj in place["objects"]:
    game["inventory"][obj] = place["objects"][obj]
    del place["objects"][obj]
    print "You picked it up"
  else:
    print_idontsee(obj) 
  return True

# drop something
def do_drop(game, place, obj):
  if obj in game["inventory"]:
    place["objects"][obj] = game["inventory"][obj]
    del game["inventory"][obj]
    print "You dropped it"
  else:
    print_idontsee(obj)
  return True

verbs = { "help": show_help, "quit": do_quit, "go": go_place, "look": look,
          "inventory": take_inventory, "get": do_get, "drop": do_drop }

msg_help = """\
  You can go places by typing: go north, go cave, or go out
  Try examining things by typing: look, look around, look bird, or look tree
  Get or stop carrying objects by typing: get box, or drop stick
  To see what you are carrying type: inventory
  You can quit any time by typing: quit

  Now go explore! Have fun!
  """

def room_look(game, place, obj):
  if obj == "mirror":
    print "you see what you saw"
    place["objects"]["saw"] = "it is a handsaw"
    return True
  return False

def room_saw(game, place, obj):
  if "saw" in game["inventory"]:
    if obj == "table":
      print "You saw the table in half. Two halves make a hole."
      place["description"] = place["description"] + "\nThere is a hole in the wall"
      place["exits"]["hole"] = "it leads out of the room"
    else:
      print "you want to saw WHAT??"
  else:
    print "you see no saw"
  return True

def room_get(game, place, obj):
  if obj == "mirror":
    print "it's affixed to the wall"
  elif obj == "table":
    print "the table is to heavy to lift"
  else:
    return False
  return True

def room_go(game, place, obj):
  if obj == "hole":
    if "hole" in place["exits"]:
      print "\nYou climb out of the hole and win the game!\nThree cheers and a tiger for you!\n"
      s = raw_input("Press any key to exit")
      exit()
    else:
      print "I see no hole"
    return True
  else:
    return False

empty = {
  "welcome": "\nNo Way Out\n\nType help if you need it",

  "help": msg_help,

  "inventory": { },

  "cur_place": "room",

  "actions": verbs,

  "places": {

    # an empty room, no place to go
    "room": { 
      "description": "You are in a tiny room with no exits. A mirror is attached "
      "to one the wall and a wooden table is in center",

      "exits": { },

      "objects": { "mirror": "",
                    "table": "its a wooden table"  },

      "actions": { "look": room_look, "saw": room_saw, "get": room_get, "go": room_go },

      "tags": { }
    }
  }
}


# This is the main loop for adventure
def start(game=empty):

  # startup game
  print game["welcome"]
  print game["places"][game["cur_place"]]["description"]

  while True:

    # get input from the user
    print "\n>",
    s = raw_input()
    obj = parse_object(s)
    verb = parse_verb(s)

    # some loop handling variables
    handled = False
    place = game["places"][game["cur_place"]]

    # first, look for specific actions
    actions = place["actions"]
    for action in actions:
      if action == verb:
        handled = actions[action](game, place, obj)

    # then look in common actions
    if not handled:
      for action in game["actions"]:
        if action == verb:
          handled = game["actions"][action](game, place, obj)

    # not handled
    if not handled:
      print "I don't know what '" + s + "' means."

if __name__ == "__main__":
  start()
