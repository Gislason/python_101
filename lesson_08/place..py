    # adventure starts in a valley
place = {    "valley": { 
      "description": "You are in a valley with tall grass and flowers. Two goats are grazing with you.",

      "exits": { "east": "bridge", "west": "blue door" },

      "objects": { "flowers": "it looks like wild rose", 
        "flower crown": "the crown is made of wild roses", "small rock": "it's a rock",
        "big rock": "it's a bid rock" },

      "actions": { },

      "tags": { }
    } }
