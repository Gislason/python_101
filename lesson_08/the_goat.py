"""
  the_goat.py

  In this adventure you are a goat trying to save your family
"""
from adventure import *


# 
def field_look(game, place, obj):

  if obj == "tree" or obj == "apple tree":

      place["objects"]["apple"] = "It's a nice delicious green apple"
    handled = True
  return handled


#------------------------------------------------------------------------------
#  The Field
#------------------------------------------------------------------------------

# my simple adventrue game
the_goat = {
  "welcome": "\nWelcome to The Goat!\n\nYou are a goat trying to save your family from the butcher.\n\n"\
             "If you get stuck, try typing help\n",

  "help": """\
  You can go places by typing: go north, or go cave
  Try examining things by typing: look, look around, look bird, or look tree
  Get or stop carrying objects by typing: get box, or drop stick
  To see what you are carrying type: inventory
  To quit, type: quit

  Now go explore! Have fun!
  """,

  "inventory": { "bell": "the bell makes a nice little sound" },

  "cur_place": "valley",

  "actions": verbs,

  "places": {

    # adventure starts in a valley
    "valley": { 
      "description": "You are in a valley with tall grass and flowers. Two goats are grazing with you.",

      "exits": { "east": "bridge", "west": "blue door" },

      "objects": { "flowers": "it looks like wild rose", 
        "flower crown": "the crown is made of wild roses", "small rock": "it's a rock",
        "big rock": "it's a bid rock" },

      "actions": { "look": meadow_look, "catch": meadow_catch, "get": meadow_get,
                    "throw": meadow_throw },

      "tags": { }
    },

    # pond has golden fish
    "pond": {
      "description": "You find yourself at a beautiful little pond with a "
      "golden fish jumping in the sunlight. A small wooden shack is "
      "at the pond's edge. A path to the west leads through a lovely copse to "
      "a meadow.",

      "exits": { "west": "meadow", "shack": "shack", "wooden shack": "shack" },

      "objects": { },

      "actions": { "look": pond_look, "fish": pond_fish, "catch": pond_catch, "get": pond_get },

      "tags": { }
    },


    # shack
    "shack": {
      "description": "You are in a small wooden fishing shack. A small net is hanging "
        "on the wall, along with a fishing pole which has no hook.",

      "exits": { "out": "pond", "door": "pond" },

      "objects": { "net": "The fishing net looks large enough to hold a small animal", 
        "fishing pole": "the fishing pole has no hook" },

      "actions": {  },

      "tags": { }
    },

    # ravine
    "ravine": {
      "description": "You are at the edge of a deep ravine. A wooden bridge crosses "
      "the ravine. You can see a small golden chest on a pedestal on the other side "
      "of the bridge. A meadow lies to the south.",

      "exits": { "south": "meadow", "bridge": "bridge" },

      "objects": { },

      "actions": { "go": ravine_go, "give": ravine_give },

      "tags": { }
    },

    # bridge
    "bridge": {
      "description": "You are on a wooden bridge over a deep ravine. Exits are north and "
        "south.",

      "exits": { "north": "pedestal", "south": "ravine" },

      "objects": { },

      "actions": { },

      "tags": { }
    },

    # pedestal
    "pedestal": {
      "description": "You are standing at a pedestal with a small golden box. A bridge "
        "stands nearby leading away from the pedestal.",

      "exits": { "bridge": "bridge" },

      "objects": { "golden box": "The golden box is locked." },

      "actions": { "open": pedestal_open, "get": pedestal_get },

      "tags": { }
    },


  }
}

if __name__ == "__main__":
  fishwish["actions"]["drink"] = do_drink

  while True:
    start(fishwish)
