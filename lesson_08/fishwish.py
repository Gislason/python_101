# adventure.py
from adventure import *

def do_drink(game, place, obj):
  if "water bottle" in game["inventory"]:
    print "ahh.. "
  else:
    print "I see nothing to drink"
  return True

msg_meadow = "You see a grassy meadow with a stump. A small path leads of to "\
             "the east through a lovely copse. A little ravine lies to the north."

# looking at the stump causes a leprechaun to dance
def meadow_look(game, place, obj):

  handled = False
  if obj == "stump":

    # a leprechaun dances
    msg_haha = "A little leprechaun dances on the stump saying 'HA HA HA'. Try and catch me!"
    print msg_haha

    tags = place["tags"]
    if not "leprechaun" in tags:
      place["description"] = msg_meadow + "\n" + msg_haha
      place["tags"]["leprechaun"] = True
    handled = True
  return handled

# if you get the rock, a key is there on the ground
def meadow_get(game, place,obj):
  tags = place["tags"]
  if obj == "stump":
    print "Aargh! Too heavy!"
    return True
  if (not "golden key" in tags) and obj == "small rock":
    place["tags"]["golden key"] = True
    place["objects"]["golden key"] = "A golden key gleams in the grass"
  return False

# if trying to catch the leprechaun
def meadow_catch(game, place, obj):
  if not (obj == "leprechaun"):
    print "that's a weird thing to catch"
  else:
    tags = place["tags"]
    if not "leprechaun" in tags:
      print "I see no leprechaun here"
    else:
      if "net" in game["inventory"]:
        print "You caught the leprechaun with your net. He vanishes in a puff of smoke,",
        print "leaving a small pot of gold behind."
        place["objects"]["pot of gold"] = "As you gaze at the gold, the gleam of avarice comes into your eye"
        place["description"] = msg_meadow
        place["tags"]["leprechaun"] = True
      else:
        print "You grab for the leprechaun and miss! Perhaps a net would help"
  return True

# throw something at the leprechaun
def meadow_throw(game, place, obj):
  if obj in game["inventory"]:
    place["objects"][obj] = game["inventory"][obj]
    del game["inventory"][obj]
    if "leprechaun" in place["tags"]:
      print "You miss! And the leprechaun laughs! HA HA HA!"
    else:
      print "You threw it"
  else:
    print_idontsee(obj)
  return True

# you win the game!
def fishwish_win():
  print "You tie the golden hook to your fishing pole with a Palomar Knot"
  print "You cast... the fish swims toward the bait... and strike! You catch it!"
  print "\nYour every wish is granted! You win! Three cheers and a tiger for you!\n"
  s = raw_input("Press enter to quit")
  exit()

# if you look at the fish
def pond_look(game, place, obj):
  if obj == "fish" or obj == "golden fish":
    print "The fish burbles out a sweet song...\n"
    print "  A golden hook you'll need"
    print "  'Tis the only thing on which I feed"
    print "  I'll grant you your every wish"
    print "  If you catch me when you say fish\n"
    return True
  elif obj == "pond":
    print "'Tis a lovely little pond"
    return True
  return False

def pond_fish(game, place, obj):
  if "fishing pole" in game["inventory"]:
    if "golden hook" in game ["inventory"]:
      fishwish_win()
    else:
      print "You need a hook for your fishing pole"
  else:
    print "You need a fishing pole to fish"
  return True

def pond_get(game, place, obj):
  if obj == "fish" or obj == "golden fish":
    print "The fish slips out of your hands"
    return True
  else:
    return False

# need the fishing pole and golden hook, then you can catch the wish fish
def pond_catch(game, place, obj):
  if obj == "fish" or obj == "golden fish":
    return pond_fish(game, place, obj)
  else:
    print "You can't catch that"
  return True

# troll stops player from going over the bridge until it gets a pot of gold
def ravine_go(game, place, obj):
  if (not "troll" in place["tags"]) and obj == "bridge":
    print "An ugly troll out from under the bridge and stops you. "\
          "'Gimme gold' he grunts."
    return True

# allow player to give pot of gold to the troll
def ravine_give(game, place, obj):
  if obj in game["inventory"]:
    if obj == "pot of gold":
      place["tags"]["troll"] = "troll"
      print "The troll takes your gold, muttering, and disappears under the bridge."
    else:
      print "The troll throws it in the ravine."
    del game["inventory"][obj]
  else:
    if obj == "goat":
      print "ha, ha... wrong story"
    else:
      print_idontsee(obj)
  return True

# open the box with the golden key
def pedestal_open(game, place, obj):
  if obj == "golden box":
    if "golden key" in game["inventory"]:
      print "You open the golden box with the golden key. Inside is a golden hook."
      place["objects"]["golden hook"] = "The golden hook gleams... and smells of fish."
      place["objects"]["golden box"] = "The box is open. Inside is a golden hook."
    else:
      print "Looks like you'll need a key"
  else:
    print "You can't open that"
  return True

# don't allow getting the golden box
def pedestal_get(game, place, obj):
  if obj == "golden box":
    print "It won't come off the pedestal"
    return True
  if obj == "pedestal":
    print "Your puny efforts don't amount to much"
    return True
  return False


# the fish wish
fishwish = {
  "welcome": "\nWelcome to Adventure!\n\nIf you get stuck, try typing help\n",

  "help": """\
  You can go places by saying: go north, or go cave
  Try examining things by saying: look, look around, look bird, or look tree
  Get or stop carrying objects by saying: get box, or drop stick
  To see what you are carrying say: inventory

  Now go explore! Have fun!
  """,

  "inventory": { "water bottle": "the bottle is full" },

  "cur_place": "meadow",

  "actions": verbs,

  "places": {

    # adventure starts in a meadow
    "meadow": { 
      "description": msg_meadow,

      "exits": { "east": "pond", "north": "ravine", "ravine": "ravine" },

      "objects": { "small rock": "you see a gleam of something under the rock", 
        "stump": "it is a stump" },

      "actions": { "look": meadow_look, "catch": meadow_catch, "get": meadow_get,
                    "throw": meadow_throw },

      "tags": { }
    },

    # pond has golden fish
    "pond": {
      "description": "You find yourself at a beautiful little pond with a "
      "golden fish jumping in the sunlight. A small wooden shack is "
      "at the pond's edge. A path to the west leads through a lovely copse to "
      "a meadow.",

      "exits": { "west": "meadow", "shack": "shack", "wooden shack": "shack" },

      "objects": { },

      "actions": { "look": pond_look, "fish": pond_fish, "catch": pond_catch, "get": pond_get },

      "tags": { }
    },


    # shack
    "shack": {
      "description": "You are in a small wooden fishing shack. A small net is hanging "
        "on the wall, along with a fishing pole which has no hook.",

      "exits": { "out": "pond", "door": "pond" },

      "objects": { "net": "The fishing net looks large enough to hold a small animal", 
        "fishing pole": "the fishing pole has no hook" },

      "actions": {  },

      "tags": { }
    },

    # ravine
    "ravine": {
      "description": "You are at the edge of a deep ravine. A wooden bridge crosses "
      "the ravine. You can see a small golden chest on a pedestal on the other side "
      "of the bridge. A meadow lies to the south.",

      "exits": { "south": "meadow", "bridge": "bridge" },

      "objects": { },

      "actions": { "go": ravine_go, "give": ravine_give },

      "tags": { }
    },

    # bridge
    "bridge": {
      "description": "You are on a wooden bridge over a deep ravine. Exits are north and "
        "south.",

      "exits": { "north": "pedestal", "south": "ravine" },

      "objects": { },

      "actions": { },

      "tags": { }
    },

    # pedestal
    "pedestal": {
      "description": "You are standing at a pedestal with a small golden box. A bridge "
        "stands nearby leading away from the pedestal.",

      "exits": { "bridge": "bridge" },

      "objects": { "golden box": "The golden box is locked." },

      "actions": { "open": pedestal_open, "get": pedestal_get },

      "tags": { }
    },


  }
}

if __name__ == "__main__":
  fishwish["actions"]["drink"] = do_drink

  while True:
    start(fishwish)
