# Python Basics Quiz

# All answers are to be written inline

# 1. Open this program in the Idle Editor

# 2. Variables. Make a variable of each type
#    a. Integer
#    b. String
#    c. Character
#    d. Decimal Number
#    e. List of 3 items
#    f. Dictionary of 3 items

# 3. Strings
#    a. Make a string that when printed displays: "I'm wonderful", he said. "Just wonderful."
#    b. Make a long string that spans 2 lines
#    c. Include the string module and use find and rfind to set the variables begin and end
#       to the first and last words of the string "Programming is my life"

# 4. Functions:
#    a. Make function that has 1 parameter that defaults to the string "hello"
#    b. Make the function print the parameter 3 times on the same line
#    c. Make the function return the first character of the parameter
#    d. Make a function that computes the square of a number

# 5. Control (If):
#     a. Make an if statement that checks if a value is greater than 5
#     b. Make an elif statement that checks if a value is greater than 10
#     c. Make an else statement that catches all the rest of the values
#     d. All three of the 

# 6. Demonstrate a while loop

# 7. Demonstrate a for loop with xrange

# 8. Make a list of names, phone numbers, and emails with at least 3 people. Append
#    another person to that list

# 9. Random numbers: Make a function that lets you guess a random number between 1 and
#    100, printing "higher" or "lower" and letting you guess again until you guess
#    it "correct". Use random.randrange()

# 10. Write a small program, anything you like, as long as it's 10 or more lines of code
