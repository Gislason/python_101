from Tkinter import Tk, Canvas, Frame, BOTH
import time

class Colors(Frame):
  
    def __init__(self, parent):
        Frame.__init__(self, parent)   
         
        self.parent = parent        
        self.initUI()
        
        
    def initUI(self):
      
        self.parent.title("Colors")        
        self.pack(fill=BOTH, expand=1)

        canvas = Canvas(self)
        canvas.create_oval(30, 10, 120, 80, 
            outline="#fb0", fill="#fb0")
        time.sleep(2)
        canvas.create_oval(150, 10, 240, 80, 
            outline="#f50", fill="#f50")
        time.sleep(2)
        canvas.create_oval(270, 10, 370, 80, 
            outline="#05f", fill="#05f")            
        time.sleep(2)
        canvas.pack(fill=BOTH, expand=1)

def main():
  
    root = Tk()
    ex = Colors(root)
    root.geometry("400x400+300+300")
    root.mainloop()  


if __name__ == '__main__':
    main()  
