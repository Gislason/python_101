import Tkinter as tk
import time

class ColoredCircles(tk.Frame):

  def __init__(self, parent):
    tk.Frame.__init__(self, parent)   
 
    self.parent = parent        
    self.initUI()
        
        
  def initUI(self):

    self.parent.title("Colors")        
    self.pack(fill=tk.BOTH, expand=1)

    # colors are #rgb, each color is 0..9,a..f, 0-f, #000 = black, #fff = white
    # (left, top, right, bottom)
    canvas = tk.Canvas(self)
    canvas.create_rectangle( 30, 10, 120, 300, outline="#000", fill="#f00")
    canvas.create_oval(150, 10, 240, 80, outline="#000", fill="#0f0")
    canvas.create_oval(270, 10, 370, 80, outline="#000", fill="#00f")            
    canvas.pack(fill=tk.BOTH, expand=1)

def main():
  root = tk.Tk()
  root.geometry("400x400")
  ex = ColoredCircles(root)
  root.mainloop()

if __name__ == '__main__':
  main()  
