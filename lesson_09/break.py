import time
 
while True:
  try:
    print('Inside while-loop')
    time.sleep(2)
  except KeyboardInterrupt:
    break
 
print('Reached end of program')
