# secret.py
import sys


def encode(s):
    encoded_s = ''
    for c in s:
        encoded_s = encoded_s + chr(ord(c)+1)
    return encoded_s


def decode(s):
    decoded_s = ''
    for c in s:
        decoded_s = decoded_s + chr(ord(c)-1)
    return decoded_s


if len(sys.argv) > 1:
    secret_string = sys.argv[1]
else:
    secret_string = 'Drink your Ovaltine'
secret = encode(secret_string)

while True:
    print("Try to decode: " + secret)
    s = input("\nEnter your guess: ")
    if s == secret_string:
        print("Hurray you win!")
        break
    else:
        print("Wrong! Try again...")
input("\nPress Enter to quit")
