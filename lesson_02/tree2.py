#!/usr/bin/env python3


def tree(branches=5, trunk=0, c='*', margin=3):
    """
           *
          ***
         *****
        *******
       *********
           *
           *
    """

    # ensure trunk/branches are reasonable
    if branches <= 0:
        branches = 5
    if trunk <= 0:
        trunk = 1 + branches // 3

    # print the branches
    for i in range(0, branches):
        print(str((1 + i*2) * c).rjust(branches+margin+i))

    # print the trunk
    for i in range(0, trunk):
        print(str(c).rjust(branches+margin))

    # print a blank line
    print('')


if __name__ == '__main__':
    print("\nMerry Christmas!\n")
    tree(5)
    input("Press Enter to quit")
