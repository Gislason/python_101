# ascii table
# American Standard Coding Information Interchange


def PrintAsciiTable(verbose=True):
    print("ASCII Table")
    if verbose:
        for i in range(32, 127):
            print(str(i).rjust(3) + "='" + chr(i) + "'", end=' ')
            if (i + 1) % 8 == 0:
                print('')
    else:
        for i in range(32, 127):
            if i % 16 == 0:
                print(str(i).rjust(3) + ':', end=' ')
            print(chr(i), end=' ')
            if (i + 1) % 8 == 0:
                print('')
    print('')


PrintAsciiTable()
