import sys
import socket
import threading
import select
import string

PORT = 6666

ENCRYPT = 0

pin = 'ABC'

def xor(string, key):
    data = ''
    for char in string:
        for ch in key:
            char = chr(ord(char) ^ ord(ch))
        data += char
    return data
	
def broadcast (sock, message, usr):
    for socket in CLIST:
        if socket != server_socket and socket != sock:
            print 'From: ', usr
            print 'Message received : ' , message
            print 'Broadcasting...\n'
            socket.send(message)

if __name__ == "__main__":

    CLIST=[]

    print '\nChatterbug Server\n'

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('', PORT))
    server_socket.listen(16)
 
    CLIST.append(server_socket)

    while True:
        read_sockets,write_sockets,error_sockets = select.select(CLIST,[],[])

        for sock in read_sockets:

            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                CLIST.append(sockfd)
                print "Client [%s, %s] connected" % addr

            else:
                try:
                    data = sock.recv(4096, )            # Data recieved from client
                except:
                    broadcast(sock, "Client (%s, %s) is offline" % addr,
                            addr)
                    print "Client (%s, %s) is offline" % addr
                    sock.close()
                    CLIST.remove(sock)
                    continue

                if data:                                # Client send data
                    if data == "q" or data == "Q":      # If client quit
                        print "Client (%s, %s) quit" % addr
                        sock.close()                    # Close socket
                        CLIST.remove(sock)              # Remove from our list
                    else:
                        broadcast(sock, data, addr)                       
                
    server_socket.close()    
