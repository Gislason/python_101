import socket
import thread
import sys

PORT        = 6666
ENCRYPT     = 0
ANSI_COLORS = 1

if ANSI_COLORS:
    ANSI_RED    = "\x1b[1;31m"
    ANSI_GREEN  = "\x1b[1;32m"
    ANSI_BLUE   = "\x1b[1;34m"
    ANSI_NORMAL = "\x1b[0m"
else:
    ANSI_RED    = ""
    ANSI_GREEN  = ""
    ANSI_BLUE   = ""
    ANSI_NORMAL = ""

pin ='ABC'

def xor(string, key):
    data = ''
    for char in string:
        for ch in key:
            char = chr(ord(char) ^ ord(ch))
        data += char
    return data

# [from_user] message
def extract_user_and_data(user, recv_data):

    # separate who message is from and the message
    n = recv_data.find(']')
    if(n > 1 and len(recv_data) >= n+2):
        from_user = recv_data[1:n]
        data = recv_data[n+2:]
    else:
        from_user = "anonymous"
        data = recv_data

    # see if it's a private message
    n = data.find(']')
    if n > 1:
        if len(data) > n+2 and data[1:n] == user:
            data = data[n+2:]
        else:
            data = ""

    return from_user, data

# Receive data from other clients connected to server
def recv_data(user):
    while True:
        try:
            recv_data = client_socket.recv(4096)       
        except:
            print "Server closed connection"
            thread.interrupt_main()
            break
        if not recv_data:
            print "Server closed connection"
            thread.interrupt_main()
            break
        else:
            if( ENCRYPT ):
                recv_data = xor(recv_data, pin)
            from_user, data = extract_user_and_data(user, recv_data)
            if not data == "":
                str = "\n" + ANSI_BLUE + from_user + " says: " + data + ANSI_NORMAL
                print str

def send_data(user):
    while True:
        send_data = str(raw_input("["+user+"] "))
        if send_data == "q" or send_data == "Q":
            client_socket.send(send_data)
            thread.interrupt_main()
            break
        else:

            if not (send_data == ""):
                send_data = "[" + user + "] " + send_data
                if( ENCRYPT ):
                    send_data = xor(send_data, pin)
                client_socket.send(send_data)

if __name__ == "__main__":

    print "\nWelcome to Chatterbug!\n"
    ip = str(raw_input("Enter server IP to connect: "))
    if ip == "":
        ip = "127.0.0.1"
    signon = "Connecting to " + ip + ":" + str(PORT) + "..."
    print signon,
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((ip, PORT))
    print "Connected.\n"

    user = str(raw_input("Enter username: "))

    print "\npress q to quit\n"

    thread.start_new_thread(recv_data,(user,))
    thread.start_new_thread(send_data,(user,))

    try:
        while True:
            continue
    except:
        print "Goodbye! Chat again sometime..."
        client_socket.close()       
