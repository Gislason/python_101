# times.py

# these are are called loops...
print("The times table, 10x10\n")
for row in range(1, 11):
    for col in range(1, 11):
        print(str(row*col).ljust(4), end=' ')
    print()

input("\nPress Enter to quit")
