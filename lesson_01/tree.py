def DrawTree():
    print("\nMerry Christmas!\n")

    # print the branches
    for i in range(0, 5):
        print(str((1 + i * 2) * '*').rjust(8 + i))

    # print the trunk
    for i in range(0, 2):
        print(str('*').rjust(8))


DrawTree()
input("\nPress Enter to quit")
