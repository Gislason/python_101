COLOR_RESET = "\x1b[0m"
COLOR_RED = "\x1b[1;31m"
COLOR_GREEN = "\x1b[1;32m"
COLOR_BLUE = "\x1b[1;34m"
COLOR_BLACK = "\x1b[1;30m"
COLOR_YELLOW = "\x1b[1;33m"

print(COLOR_RED + "This is red")
print(COLOR_GREEN + "This is green")
print(COLOR_BLUE + "This is blue")
print(COLOR_BLACK + "This is black")
print(COLOR_YELLOW + "This is yellow")
print(COLOR_RESET + "Back to normal")
