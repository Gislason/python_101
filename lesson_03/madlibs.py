# madlibs.py
import sys
import string


def get_fields(story_name):
    """ get fields to fill in with funny values (e.g. nouns, verbs, names, etc...) """
    fields = []
    fd = open(story_name)
    for line in fd:
        i = 0
        while True:
            i = line.find('[', i)
            if i < 0:
                break
            e = line.find(']', i)
            if e < 0:
                break
            s = line[i:e + 1]
            if s not in fields:
                fields.append(s)
            i = e + 1
    fd.close()
    return fields


def get_values(fields):
    """ # get the values for each field (e.g. nouns, verbs, names, numbers, et...) """
    values = []
    for field in fields:
        s = input("enter " + field + ": ")
        values.append(s)
    return values


def say_story(story_name, fields, values):
    """ print out a story with a list of fields and values (substitutions) """
    fd = open(story_name)
    for line in fd:
        for i in range(len(fields)):
            line = line.replace(fields[i], values[i])
        print(line, end=' ')
    fd.close()
    print("\n\n")


def madlib(story_name):
    """ madlibs """
    print("\nWelcome to Madlibs\n\n")
    fields = get_fields(story_name)
    values = get_values(fields)
    say_story(story_name, fields, values)


# main program
if __name__ == "__main__":
    if len(sys.argv) > 1:
        story_name = sys.argv[1]
    else:
        story_name = "story.txt"
    madlib(story_name)
