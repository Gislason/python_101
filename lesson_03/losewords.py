# losewords.py


def lose_one_word(sentence):
    """ remove the first word of the sentence. Return None if last word. """

    i = sentence.find(' ')
    if i < 0:
        return None
    return sentence[i + 1:]


def lose_words(sentence="goodbye cruel world"):
    """ remove words one by one until gone... """
    print("\nlosing words... one by one\n")
    while sentence is not None:
        print(sentence)
        sentence = lose_one_word(sentence)
    print("\n*poof* ... gone!\n")


lose_words("This sentence will soon disappear, word by word!")
