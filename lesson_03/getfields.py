# getfields.py


def get_fields(filename):
    """ get [fields] in file """
    fields = []
    fd = open(filename)
    for line in fd:
        i = 0
        while True:
            i = line.find('[', i)
            if i < 0:
                break
            e = line.find(']', i)
            if e < 0:
                break
            s = line[i:e + 1]
            if s not in fields:
                fields.append(s)
            i = e + 1
    fd.close()
    return fields


fields = get_fields('haha.txt')
print(fields)
