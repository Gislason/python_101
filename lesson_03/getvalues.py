# getvalues.py


def get_values(fields):
    """ get the values for each field (e.g. nouns, verbs, names, numbers, etc...) """
    values = []
    for field in fields:
        s = input("enter " + field + ": ")
        values.append(s)
    return values


values_list = get_values(['name', 'number', 'color'])
print("you entered: ", values_list)
