import sys
import socket
import random
from threading import Thread
from datetime import datetime

COLOR_RESET = "\x1b[0m"
COLOR_RED = "\x1b[1;31m"
COLOR_GREEN = "\x1b[1;32m"
COLOR_BLUE = "\x1b[1;34m"
COLOR_BLACK = "\x1b[1;30m"
COLOR_YELLOW = "\x1b[1;33m"

# set the available colors
colors = [COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_BLACK, COLOR_YELLOW]

# choose a random color for the client
client_color = random.choice(colors)

# server's IP address and port
if len(sys.argv) > 1:
    SEVER_HOST = sys.argv[1]
else:
    SERVER_HOST = "127.0.0.1"
SERVER_PORT = 5002
separator_token = "<SEP>"  # we will use this to separate the client name & message

# initialize TCP socket
s = socket.socket()
print(f"[*] Connecting to {SERVER_HOST}:{SERVER_PORT}...")
# connect to the server
s.connect((SERVER_HOST, SERVER_PORT))
print("[+] Connected.")

# prompt the client for a name
name = input("Enter your name: ")


def listen_for_messages():
    while True:
        message = s.recv(1024).decode()
        print("\n" + message)
        print("Say: ", end='', flush=True)


# make a thread that listens for messages to this client & print them
t = Thread(target=listen_for_messages)
# make the thread daemon so it ends whenever the main thread ends
t.daemon = True
# start the thread
t.start()

while True:
    # input message we want to send to the server
    to_send = input("Say: ")
    # a way to exit the program
    if to_send.lower() == 'q' or to_send.lower() == 'quit':
        break
    # add the datetime, name & the color of the sender
    date_now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    to_send = f"{client_color}[{date_now}] {name}{separator_token}{to_send}{COLOR_RESET}"
    # finally, send the message
    s.send(to_send.encode())

# close the socket
s.close()
