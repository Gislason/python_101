# Binary, converts between binary and decimal


def convert(s):
    """ convert a string of any base to a decimal number (e.g. 123, 0x7B, 0b1111011, 0zX3) """

    # determine base
    base = 10
    if s[:2] == '0b':
        base = 2
    if s[:2] == '0x':
        base = 16
    if s[:2] == '0z':
        base = 12
        s = dozenal(s[2:])

    return int(s, base), base


def dozenal(s):
    s2 = ''
    for c in s:
        if (c == 'X') or (c == 'x'): c = 'a'
        if (c == 'E') or (c == 'e'): c = 'b'
        s2 = s2 + c
    return s2


def basename(base):
    if base == 2: 
        strbase = "binary"
    elif base == 10: 
        strbase = "decimal"
    elif base == 12:
        strbase = "dozenal"
    elif base == 16:
        strbase = "hex"
    else:
        strbase = "base " + str(base)
    return strbase


if __name__ == "__main__":
    while True:
        s = input("Enter q to quit, or a number in any base (e.g. 123, 0x1E, 0b101, 0z14X): ")
        if s[0] == 'q':
            break
        n, base = convert(s)
        print(s + " in " + basename(base) + " is " + str(n) + "\n")
