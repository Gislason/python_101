# Morse Code

# morse letters
morseA = ".-"
morseB = "-..."
morseC = "-.-."
morseD = "-.."
morseE = "."
morseF = "..-."
morseG = "--."
morseH = "...."
morseI = ".."
morseJ = ".---"
morseK = "-.-"
morseL = ".-.."
morseM = "--"
morseN = "-."
morseO = "---"
morseP = ".--."
morseQ = "--.-"
morseR = ".-."
morseS = "..."
morseT = "-"
morseU = "..-"
morseV = "...-"
morseW = ".--"
morseX = "-..-"
morseY = "-.--"
morseZ = "--.."
morse_letters = [ morseA, morseB, morseC, morseD, morseE, \
                  morseF, morseG, morseH, morseI, morseJ, \
                  morseK, morseL, morseM, morseN, morseO, \
                  morseP, morseQ, morseR, morseS, morseT, \
                  morseU, morseV, morseW, morseX, morseY, \
                  morseZ ]

# morse numbers
morse0 = "-----"
morse1 = ".----"
morse2 = "..---"
morse3 = "...--"
morse4 = "....-"
morse5 = "....."
morse6 = "-...."
morse7 = "--..."
morse8 = "---.."
morse9 = "----."
morse_numbers = [ morse0, morse1, morse2, morse3, morse4, \
                  morse5, morse6, morse7, morse8, morse9 ]

# converts a string to Morse Code
def morse(str):
  morse_str = ''
  for c in str:
    ascii_c = ord(c)
    if ascii_c >= ord('a') and ascii_c <= ord('z'):
      morse_str = morse_str + morse_letters[ascii_c - ord('a')] + ' '
    if ascii_c >= ord('A') and ascii_c <= ord('Z'):
      morse_str = morse_str + morse_letters[ascii_c - ord('A')] + ' '
    if ascii_c >= ord('0') and ascii_c <= ord('9'):
      morse_str = morse_str + morse_number[ascii_c - ord('0')] + ' '
    if ascii_c == ord(' '):
      morse_str = morse_str + "/ "
  return morse_str

def table(cols=8):
  i = 0
  for l in morse_letters:
    s = chr(ord('A')+i) + " = " + str(l)
    print s.ljust(9),
    i += 1
    if i > 0 and (i % cols) == 0:
      print ""

def numbers(cols=5):
  i = 0
  for l in morse_numbers:
    s = chr(ord('0')+i) + " = " + str(l)
    print s.ljust(10),
    i += 1
    if i > 0 and (i % cols) == 0:
      print ""

if __name__ == '__main__':
  print "\nCan you guess the following morse code?\n"
  print morse('Drink your Ovaltine')
  s = raw_input("\nPress any key to exit")
