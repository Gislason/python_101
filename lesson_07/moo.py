# Bulls and Cows
import random
import string

PIECES      = "0123456789"

# random.randint(1,100)

def moo_select_pieces(num_pieces):
  game = []
  for i in range(0,num_pieces):
    game.append(random.choice(PIECES))
  return game


def moo_play_game(num_pieces=4):
  """
  The game of Moo is a guessing game. Moo will pick a 4 digit number.
  You guess the digits.
  Each digit that is right, but in the wrong place is a cow.
  Each digit that is right (and in the right place) is a bull.
  Get all bulls and you win!
  """

  print "\nWelcome to Moo!\n\nGuess the " + str(num_pieces) + " hidden numbers (e.g. 3053).\n"
  print "Each correct digit earns you a bull, each correct digit in the wrong place earns you a cow."
  print "\ntype 'q' to quit\n"

  max_turns = 8 + num_pieces
  game = moo_select_pieces(num_pieces)
  quit = False

  for turn in range(1, max_turns+1):

    # initialize variables per turn
    leftover = []
    g2       = []
    bulls = 0
    cows  = 0

    # get a guess
    while not quit:
      guess = raw_input(str(turn) + ") enter a " + str(num_pieces) + " digit guess: ")
      if guess == 'q':
        quit = True
      elif guess == 'peek':
        print game
      elif len(guess) == num_pieces:
        break
      elif len(guess) < num_pieces:
        print "enter more digits"
      elif len(guess) > num_pieces:
        print "enter less digits"

    # quitting
    if quit:
      break

    # find bulls
    for i in range(0,num_pieces):
      if guess[i] == game[i]:
        bulls = bulls + 1
      else:
        g2.append(guess[i])
        leftover.append(game[i])

    # find cows
    for i in range(0,len(g2)):
      if g2[i] in leftover:
        cows = cows + 1
        leftover.remove(g2[i])

    # initialize margin for proper display
    if turn < 10:
      margin = 3*" "
    else:
      margin = 4*" "
    if cows == 1:
      cowstr = " cow"
    else:
      cowstr = " cows"
    if bulls == 1:
      bullstr = " bull"
    else:
      bullstr = " bulls"

    # display what user found
    if bulls == num_pieces:
      break
    elif (cows == 0) and (bulls == 0):
      print margin + "Bad luck. Try other numbers"
    else:
      print margin + str(cows) + cowstr + " and " + str(bulls) + bullstr

  # done (either out of turns, or got it right)
  if quit:
    print "Giving up? How now, brown cow?\n"
  elif bulls == num_pieces:
    print "Moo! Let the bells ring out and the banners fly, you have won!\n"
  else:
    print "Moo who who! You lost. The number was " + str(game) + "\n"

# main entry point
if __name__ == "__main__":
  random.seed()
  s = 'y'
  while s=='y' or s=='Y' or s=='yes':
    moo_play_game()
    s = raw_input("Do you want to play again (y/n)? ")
