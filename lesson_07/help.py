import pyaudio
import numpy as np
import time

p = pyaudio.PyAudio()

volume = 0.50    # range [0.0, 1.0]
fs = 44100       # sampling rate, Hz, must be integer
duration = 0.2   # in seconds, may be float
f = 440.0        # sine frequency, Hz, may be float

# generate samples, note conversion to float32 array
sample_short = (np.sin(2*np.pi*np.arange(fs*duration)*f/fs)).astype(np.float32)
sample_long  = (np.sin(2*np.pi*np.arange(fs*duration*3)*f/fs)).astype(np.float32)

# for paFloat32 sample values must be in range [-1.0, 1.0]
stream = p.open(format=pyaudio.paFloat32,
                channels=1,
                rate=fs,
                output=True)

# play. May repeat with different volume values (if done interactively) 
for i in xrange(0,3):
  stream.write(volume*sample_short)
  time.sleep(duration)
time.sleep(.4)
for i in xrange(0,3):
  stream.write(volume*sample_long)
  time.sleep(duration*2)
time.sleep(.4)
for i in xrange(0,3):
  stream.write(volume*sample_short)
  time.sleep(duration)

stream.stop_stream()
stream.close()

p.terminate()
