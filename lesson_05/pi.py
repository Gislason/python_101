import sys

# anybody understand the math?
def pi_digit(n):
    q, r, t, k, m, x = 1, 0, 1, 1, 3, 3
    for j in range(n*4):
        if 4 * q + r - t < m * t:
            yield m
            q, r, t, k, m, x = 10*q, 10*(r-m*t), t, k, (10*(3*q+r))//t - 10*m, x
        else:
            q, r, t, k, m, x = q*k, (2*q+r)*x, t*x, k+1, (q*(7*k+2)+r*x)//(t*x), x+2

# make a string of pi to n digits
def pi_string(n):
  my_array = []
  for i in pi_digit(n):
      my_array.append(str(i))
  if n > 1:
    my_array = my_array[:1] + ['.'] + my_array[1:]
  s = "".join(my_array)
  return s

# main program
if __name__ == "__main__":
  if len(sys.argv) > 1:
    n = int(sys.argv[1])
  else:
    n = 100
  s = pi_string(n)
  print "pi to " + str(n) + " digits: " + s
