# Random DNA

import random
import string

def GetMyDna(count):
  dna = "GCAT"
  myDna = ""
  for i in xrange(1,count):
    n = random.randint(0,len(dna)-1)
    myDna = myDna + dna[n]
  return myDna

if __name__ == "__main__":
  print "Random DNA Sequencing is composed of:"
  print "  G - guanine"
  print "  C - cytosine"
  print "  A - adenine"
  print "  T - thymine\n"
  print "My (random) DNA is:",
  print GetMyDna(166)
