# Python Basics Quiz

# All answers can be inline

# 1. Open the Python document tutorial.pdf. Look over the table of contents.
#    Enter the Fibonacci series program in section 3.2
#    Find a topic that interests you for discussion in class (write it down as
#    the answer to this question).

# 2. Make and print a variable of each type: integer, decimal (like pi), string, character

# 3. Make a function that prints "Bye" any number of times.

# 4. Make a string that when printed displays: "I'm wonderful", he said. "Just wonderful."

# 5. Make a function that slices a string into two equal parts and returns both parts

# 6. Make a function that computes the square of a number

# 7. Demonstrate a while loop

# 8. Demonstrate a for loop with range

# 9. Make a list of names, phone numbers, and emails with at least 3 people. Append
#    another person to that list

# 10. Make a function that lets you guess a random number between 1 and 100, printing
#     "higher" or "lower" and letting you guess again until you guess it "correct"

# 11. Look at and run each of dna.py, factorial.py, prime.py and pi.py. What do they do?

# 12. Extra credit if you can explain the math and program in pi.py
